drawWordCloud = (word_count) => {
  let svg_location = "#chart";
  let width = 1200;
  let height = 500;
  let fill = d3.scale.category20();
  let word_entries = d3.entries(word_count);
  let xScale = d3.scale.linear()
    .domain([0, d3.max(word_entries, function(d) {
        return d.value;
      })
    ])
    .range([10,100]);

  draw = (words) => {
    d3.select(svg_location).append("svg")
        .attr("width", width)
        .attr("height", height + 100)
      .append("g")
        .attr("transform", "translate(" + [width >> 1, height >> 1] + ")")
      .selectAll("text")
        .data(words)
      .enter().append("text")
        .style("font-size", function(d) { return xScale(d.value) + "px"; })
        .style("fill", function(d, i) { return fill(i); })
        .attr("text-anchor", "middle")
        .attr("transform", function(d) {
          return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
        })
        .text(function(d) { return d.key; });
  }

  d3.layout.cloud().size([width, height])
    .timeInterval(20)
    .words(word_entries)
    .padding(3)
    .fontSize(function(d) { return xScale(+d.value); })
    .text(function(d) { return d.key; })
    .rotate(function() { return ~~(Math.random() * 2) * 90; })
    .on("end", draw)
    .start();

  d3.layout.cloud().stop();
}