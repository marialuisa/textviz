function getDataSigma(enterData) {
  
  let max = 8;
  let hashNodes = {};
  let hashTargetColors = {};
  let targetNodes = 0;
  let nodeId = 0;
  let edgeId = 0;
  let hashEdgeTarget = {};
  let hashColors = {};
  let jsonSigma = {
    nodes: [],
    edges: []
  };

  random_rgba = () => {
    var o = Math.round, r = Math.random, s = 255;
    let c1 = o(r()*s);
    let c2 = o(r()*s);
    let c3 = o(r()*s);
    return ['rgba(' + c1 + ',' + c2 + ',' + c3 + ',' + 1 + ')',
      'rgba(' + c1 + ',' + c2 + ',' + c3 + ',' + .6 + ')'];
  }

  const buildColor = () => {
    let hexadecimais = '0123456789ABCDEF';
    let cor = '#';
    for (let i = 0; i < 6; i++ ) {
        cor += hexadecimais[Math.floor(Math.random() * 16)];
    }
    return cor;
  }

  const buildNode = (nId, label, size, color) => ({
    "id": "n"+ nId,
    "label": label,
    "size": size, 
    "color": color,
    "x": Math.random(),
    "y": Math.random()
  });

  const addNode = (node) => {
    jsonSigma.nodes.push(node);
  };

  const newNode = (label, size, color) => {
    if(!hashNodes[label]) {
      let nId = nodeId;
      let dynamicColor = random_rgba();
      if(!color) color = dynamicColor[0];
      addNode(buildNode(nId, label, size, color));
      hashNodes[label] = nId;
      if(!hashColors[label]) hashColors[label] = dynamicColor;
      nodeId++;
    }
  };

  const buildEdge = (eId, source, target, color) => ({
    "id": "e"+ eId,
    "source": "n"+ source,
    "target": "n"+ target,
    "color": color
  });

  const addEdge = (edge) => {
    jsonSigma.edges.push(edge);
  };

  const newEdge = (source, target, color) => {
    let eId = edgeId;
    if(!color) color = '#aaa';
    addEdge(buildEdge(eId, source, target, color));
    edgeId++;
  };

  enterData.forEach(function(value) {
    newNode(value.termId, 4);
  });
  enterData.forEach(function(node, id) {
    node.similares.forEach(function(node_similar, node_id) {
      let colorc = hashColors[node.termId] || random_rgba();
      hashColors[node_similar] = colorc;
      newNode(node_similar, 2, colorc[0]);
      newEdge(hashNodes[node.termId], hashNodes[node_similar], colorc[1]);
    });
  });

  return jsonSigma;
}
