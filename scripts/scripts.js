let wMap;
let totalWords = 0;
let totalCaracteres = 0;

window.onload = function () {
  showContentText = (e, fileSelected) => {
    const fileExtension = /text.*/;
    const fileTobeRead = fileSelected.files[0];
    if (fileTobeRead.type.match(fileExtension)) {
      const fileReader = new FileReader();
      fileReader.onload = function (e) {
        const fileContents = document.getElementById('filecontents');
        const countW = document.getElementById('count-words');
        const countC = document.getElementById('count-caracteres');
        let data = fileReader.result;
        let wordsArray = splitByWords(data);
        let wordsMap = createWordMap(wordsArray);
        wMap = wordsMap;
        fileContents.innerText = fileReader.result;
        countW.innerText = totalWords;
        countC.innerText = totalCaracteres;
      }
      fileReader.readAsText(fileTobeRead);
    }
  }

  let frm = $('#form');
  let form = $('#form')[0];
  frm.submit(function (ev) {
    $('.loading').show();
    $('.loading').css('display','block');
    $('#submit-btn').hide();
    $('#chart').html("");
    $('#container-sigma').html("");
    $('.visualizations').hide();
    $.ajax({
      type: frm.attr('method'),
      url: frm.attr('action'),
      data: new FormData(form),
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      cache: false,
      success: function (data) {
        generateCharts();
      },
      error: function(r) {
        console.log('error', r);
      }
    });

    ev.preventDefault();
  });

  $('input:file').change(function(e) {
    let arq = this.files[0];
    const maxSize = 10000;
    const hasError = arq.size > maxSize || arq.type !== 'text/plain';

    if(hasError) {
      $('#submit-btn').hide();
      if(arq.size > maxSize) $('#error-size').show();
      if(arq.type !== 'text/plain') $('#error-type').show();
      $('#content-text').hide();
    } else {
      $('#submit-btn').show();
      $('.error').hide();
      $('#content-text').show();
      showContentText(e, this);
    }
  });
}

splitByWords = (text) => {
  let wordsArray = text.split(/\s+/);
  let lowerCaseArray = [];
  wordsArray.forEach((word) => {
    const lowerWord = word.toLowerCase();
    if(!stopwords.hasOwnProperty(lowerWord)) {
      lowerCaseArray.push(lowerWord);
    }
  });
  totalWords = wordsArray.length;
  totalCaracteres = text.length;
  return lowerCaseArray;
}

createWordMap = (wordsArray) => {
  let wordsMap = {};
  wordsArray.forEach(function (key) {
    if (wordsMap.hasOwnProperty(key)) {
      wordsMap[key]++;
    } else {
      wordsMap[key] = 1;
    }
  });
  return wordsMap;
}

sortByCount = (wordsMap) => {
  let finalWordsArray = [];
  finalWordsArray = Object.keys(wordsMap).map(function (key) {
    return {
      name: key,
      total: wordsMap[key]
    };
  });
  finalWordsArray.sort(function (a, b) {
    return b.total - a.total;
  });
  return finalWordsArray;
}

removeUnnecessaryWords = (wordsMap) => {
  let finalWordsMap = {};
  Object.keys(wordsMap).forEach(function (key) {
    if(wordsMap[key] > 1) {
      finalWordsMap[key] = wordsMap[key];
    }
  });
  return finalWordsMap;
}

generateCharts = () => {
  $('#submit-btn').hide();
  $('.visualizations').show();
  $('.visualizations').css('display','block');
  $('.loading').hide();
  let finalWordsMap = removeUnnecessaryWords(wMap);
  drawWordCloud(finalWordsMap);
  getWordsW2v();
}

getWordsW2v = () => {
  let finalWordsArray = sortByCount(wMap);
  let mostFrequence = finalWordsArray.slice(0, 20);
  let stringWords = "";
  mostFrequence.forEach((w, i) => {
    if(w.name.length > 1) {
      if(i > 0) {
        stringWords += ",";
      }
      stringWords += w.name;
    }
  });

  const url = 'https://biible.herokuapp.com/similarwordstextarray?terms='
      + stringWords + '&zise=30&cos=true';

  d3.json(url, function(data) {
    drawForceGraph(data);
  });
}

drawForceGraph = (enterData) => {
  const dataSigma = getDataSigma(enterData);
  s = new sigma({
    graph: dataSigma,
    renderer: {
      container: document.getElementById('container-sigma'),
      type: 'canvas'
    },
    style:{ 
      maxWidth: "inherit", 
      width: "100%", 
      height: "600px" 
    },
    settings: {
      edgeLabelSize: 'proportional',
      defaultEdgeType: "curvedArrow",
      drawEdges: true,
      clone: false,
      labelThreshold: 0,
      font: 'Nunito',
      drawEdgeLabels: false,
      scalingMode: 'outside',
      sideMargin: 1,
      defaultEdgeArrow: 'target',
      borderSize: 2,
      minArrowSize: 5
    }
  });
}